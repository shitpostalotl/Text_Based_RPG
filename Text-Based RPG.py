# Imports
import time
import random


# Specaltxt
def sp_print(message):
    for letter in message:
        print(letter, end="", flush=True)
        time.sleep(.1)
    print()


def main():
    Invantory = []

    # loging
    rooms = [[6, 8, "1E", "0S"], [6, 4, "0N", "2E"], [12, 12, "-6W", "0N"], [10, 10, "-5E"], [6, 8, "2S", "0N"],
             [6, 8, "1W", "3E"], [6, 8, "0S", "3W"], [20, 18, "0E", "5W"], [20, 24, "0W"]]

    NPCs = [[8, -11, -8], [8, 0, -8], [8, 11, -8], [8, -11, 8], [8, 0, 8], [8, 11, 8], [1, 0, -2]]

    chests = [[8, -2, -3], [8, 5, 7], [7, -2, -2]]

    ATK = 1
    DEF = 1

    world = [[0, 5, 6],
             [4, 3, 2],
             [1, 7, 8],
             ]

    current_room = [2, 1]

    battle = False

    sp_print("Welcome to my text based RPG!")

    # Name
    sp_print("Please name your carictor")
    Name = input()
    if Name == "No":
        Name = ("Smart-aleck")
    if Name == "No.":
        Name = ("Smart-aleck")
    if Name == "no":
        Name = ("Smart-aleck")

    # Starting location
    LocationX = 0
    LocationY = 0

    NMY = [4, 0, 5, 6, 2]
    PHelth = 10
    # Boundries
    MaxF = 11
    MaxB = 11
    MaxL = 11
    MaxR = 11

    while True:

        # Locations
        sp_print("Locations")
        print("Player: " + str(LocationX) + "," + str(LocationY))
        for npc in NPCs:
            if npc[0] == world[current_room[0]][current_room[1]]:
                print("NPC: " + str(npc[1]) + "," + str(npc[2]))
        for chest in chests:
            if chest[0] == world[current_room[0]][current_room[1]]:
                print("Chest: " + str(chest[1]) + "," + str(chest[2]))

        room = rooms[world[current_room[0]][current_room[1]]]
        for door in room[2:]:
            if "W" in door:
                print("Door: " + str(-room[1] // 2) + "," + door[:-1])
            if "E" in door:
                print("Door: " + str(room[1] // 2) + "," + door[:-1])
            if "N" in door:
                print("Door: " + door[:-1] + "," + str(room[2] // 2))
            if "S" in door:
                print("Door: " + door[:-1] + "," + str(-room[2] // 2))

        # Movement
        print("Forward (F), Backward (B), Left (L), or Right (R)?")
        Move = input()
        if Move == "F":
            if LocationY + 1 < MaxF:
                LocationY += 1
            else:
                print(Name + ", due to the natre of the game, coudn't see the wall, and bonked into it.")
        if Move == "B":
            if LocationY - 1 > -MaxB:
                LocationY -= 1
            else:
                print(Name + ", due to the natre of the game, coudn't see the wall, and bonked into it.")
        if Move == "L":
            if LocationX + 1 < MaxL:
                LocationX += 1
            else:
                print(Name + ", due to the natre of the game, coudn't see the wall, and bonked into it.")
        if Move == "R":
            if LocationX - 1 > -MaxR:
                LocationX -= 1
            else:
                print(Name + ", due to the natre of the game, coudn't see the wall, and bonked into it.")
        # Npc interact
        for npc in NPCs:
            if npc[0] == world[current_room[0]][current_room[1]]:
                if npc[1] == LocationX and npc[2] == LocationY:
                    sp_print("Talk to the NPC? Say 'Y' for yes")
                    Interact = input()
                    if Interact == "Y":
                        # NPC Dialog
                        if NPCs.index(npc) == 0:
                            sp_print("Where evien ARE we?")
                        if NPCs.index(npc) == 1:
                            sp_print("I counted 7 people when we came here!")
                        if NPCs.index(npc) == 2:
                            sp_print("Are you " + Name + "?")
                        if NPCs.index(npc) == 3:
                            sp_print("Do you want to sell?")
                        if NPCs.index(npc) == 4:
                            sp_print("Where is the guy thet left?")
                        if NPCs.index(npc) == 5:
                            sp_print("What is the Wifi pasword?")
                        if NPCs.index(npc) == 6:
                            sp_print("I only left because the others where anoying. Like, very anoying.")

        # room transition
        room = rooms[world[current_room[0]][current_room[1]]]
        for door in room[2:]:
            if "W" in door:
                if LocationX == -room[1] // 2 and LocationY == int(door[:-1]):
                    sp_print("Go through the door? Say 'Y' for yes")
                    Interact = input()
                    if Interact == "Y":
                        battle = True
                        current_room[1] -= 1
                        room = rooms[world[current_room[0]][current_room[1]]]
                        for door in room[2:]:
                            if "E" in door:
                                LocationX = room[0] // 2
                                LocationY = int(door[:-1])
            if "E" in door:
                if LocationX == room[1] // 2 and LocationY == int(door[:-1]):
                    sp_print("Go through the door? Say 'Y' for yes")
                    Interact = input()
                    if Interact == "Y":
                        battle = True
                        current_room[1] += 1
                        room = rooms[world[current_room[0]][current_room[1]]]
                        for door in room[2:]:
                            if "W" in door:
                                LocationX = -room[0] // 2
                                LocationY = int(door[:-1])
            if "N" in door:
                if LocationX == int(door[:-1]) and LocationY == room[2] // 2:
                    sp_print("Go through the door? Say 'Y' for yes")
                    Interact = input()
                    if Interact == "Y":
                        battle = True
                        current_room[0] -= 1
                        room = rooms[world[current_room[0]][current_room[1]]]
                        for door in room[2:]:
                            if "S" in door:
                                LocationX = int(door[:-1])
                                LocationY = -room[1] // 2
            if "S" in door:
                if LocationX == int(door[:-1]) and LocationY == -room[2] // 2:
                    sp_print("Go through the door? Say 'Y' for yes")
                    Interact = input()
                    if Interact == "Y":
                        battle = True
                        current_room[0] += 1
                        room = rooms[world[current_room[0]][current_room[1]]]
                        for door in room[2:]:
                            if "N" in door:
                                LocationX = int(door[:-1])
                                LocationY = room[1] // 2
        # Chest Drops
        for chest in chests:
            if chest[0] == world[current_room[0]][current_room[1]]:
                if chest[1] == LocationX and chest[2] == LocationY:
                    sp_print("Open the chest? Say 'Y' for yes")
                    Interact = input()
                    if Interact == "Y":
                        # Chest Dialog
                        if chests.index(chest) == 0:
                            sp_print("Its a stiff glove. (+3 ATK)")
                            Invantory.append("Glove")
                            ATK += 3
                            chests.remove(chest)
                        if chests.index(chest) == 1:
                            sp_print("Its a... bullet resistant vest! (+5 DEF)")
                            Invantory.append("Vest")
                            DEF += 5
                            chests.remove(chest)
                        if chests.index(chest) == 2:
                            sp_print("Its a note. It reads:")
                            sp_print("To " + Name + ",")
                            sp_print("Meet me in room 3. The fate of this world will be decided.")
                            sp_print("My condolences,")
                            sp_print("  No-one")
                            sp_print("P.S. Also, EXACTLY 10 HELTHPAKS!")
                            sp_print("P.P.S. In the middle of room 3.")
                            Invantory.append("Note")
                            Invantory.extend(["Helthpak", "Helthpak", "Helthpak", "Helthpak", "Helthpak", ])
                            Invantory.extend(["Helthpak", "Helthpak", "Helthpak", "Helthpak", "Helthpak", ])
                            chests.remove(chest)

        if room in NMY and battle:
            battle = False
            coin = random.randint(1, 2)
            EHelth = 10
            EDefence = random.randint(1, 5)
            EAtack = random.randint(1, 4)
            EHeals = 3
            sp_print("BATTLE")
            sp_print("  START!")
            if "Vest" in Invantory:
                sp_print("enemy: You protect yourself, so you fear dieing. But you will always respawn in room 7.")
            if "Glove" in Invantory:
                sp_print("enemy: That glove that you are wearing... That must be uncomfortable.")
            while True:
                if coin % 2 == 1:
                    sp_print("Player Health " + PHelth)
                    sp_print("Opponent Health " + EHelth)
                    sp_print("[F]ight")
                    sp_print("[H]eal")
                    BReactiton = input()
                    if BReactiton == "F":
                        chance = random.randint(1, 100)
                        chance = chance + ATK * 2 - EDefence * 2
                        if 1 <= chance <= 10:
                            sp_print("You missed.")
                        if 11 <= chance <= 30:
                            sp_print("You hit, but just barely.")
                            EHelth -= 1
                        if 31 <= chance <= 70:
                            sp_print("You hit.")
                            EHelth -= 2
                        if 71 <= chance <= 90:
                            sp_print("You hit well.")
                            EHelth -= 3
                        if 91 <= chance <= 100:
                            sp_print("You hit, in a critical fashion.")
                            EHelth -= 5
                    else:
                        if "Helthpak" in Invantory:
                            Invantory.remove("Helthpak")
                            PHelth += 3
                        else:
                            sp_print("You ran out of healthpaks.")
                    if EHelth < 1:
                        sp_print("Instead of dying, the enemy just... Disapeared")
                        break
                if coin % 2 == 0:
                    if EHelth < 3 and EHeals > 0:
                        EHeals -= 1
                        EHelth += 3
                    else:
                        chance = random.randint(1, 100)
                        chance = chance + EAtack * 2 - DEF * 2
                        if 1 <= chance <= 10:
                            sp_print("Enemy missed.")
                        if 11 <= chance <= 30:
                            sp_print("Enemy hit, but just barely.")
                            PHelth -= 1
                        if 31 <= chance <= 70:
                            sp_print("Enemy hit.")
                            PHelth -= 2
                        if 71 <= chance <= 90:
                            sp_print("Enemy hit well.")
                            PHelth -= 3
                        if 91 <= chance <= 100:
                            sp_print("Enemy hit, in a critical fashion.")
                            PHelth -= 5
                    if PHelth < 1:
                        sp_print("You died.")
                        main()
                coin += 1

        if room == 3:
            battle = False
            coin = random.randint(1, 2)
            EHelth = 10
            EDefence = random.randint(1, 5)
            EAtack = random.randint(1, 4)
            EHeals = 5
            sp_print("BOSS")
            sp_print(" BATTLE")
            sp_print("   START!")
            if "Note" in Invantory:
                sp_print("Boss: You found my note. You know what must be done, or they will lose interest.")
            while True:
                if coin % 2 == 1:
                    sp_print("Player Health " + PHelth)
                    sp_print("Opponent Health " + EHelth)
                    sp_print("[F]ight")
                    sp_print("[H]eal")
                    BReactiton = input()
                    if BReactiton == "F":
                        chance = random.randint(1, 100)
                        chance = chance + ATK * 2 - EDefence * 2
                        if 1 <= chance <= 10:
                            sp_print("You missed.")
                        if 11 <= chance <= 30:
                            sp_print("You hit, but just barely.")
                            EHelth -= 1
                        if 31 <= chance <= 70:
                            sp_print("You hit.")
                            EHelth -= 2
                        if 71 <= chance <= 90:
                            sp_print("You hit well.")
                            EHelth -= 3
                        if 91 <= chance <= 100:
                            sp_print("You hit, in a critical fashion.")
                            EHelth -= 5
                    else:
                        if "Helthpak" in Invantory:
                            Invantory.remove("Helthpak")
                            PHelth += 3
                        else:
                            sp_print("You ran out of healthpaks.")
                    if EHelth < 1:
                        sp_print("Instead of dying, the enemy just... Left the room?")
                        break
                if coin % 2 == 0:
                    if EHelth < 3 and EHeals > 0:
                        EHeals -= 1
                        EHelth += 3
                    else:
                        chance = random.randint(1, 100)
                        chance = chance + EAtack * 2 - DEF * 2
                        if 1 <= chance <= 10:
                            sp_print("Boss used PATIENCE.")
                        if 11 <= chance <= 30:
                            sp_print("Boss used STARE.")
                            PHelth -= 1
                        if 31 <= chance <= 70:
                            sp_print("Boss used SLAP.")
                            PHelth -= 2
                        if 71 <= chance <= 90:
                            sp_print("Boss used REALITY SCRATCH.")
                            PHelth -= 3
                        if 91 <= chance <= 100:
                            sp_print("Boss used Ω COLAPSE.")
                            PHelth -= 5
                    if PHelth < 1:
                        sp_print("You where Un-existed.")
                        main()
                coin += 1
main()
